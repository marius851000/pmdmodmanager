from PyQt5.QtWidgets import QLabel, QVBoxLayout, QGroupBox, QLineEdit, QPushButton, QCheckBox

from qtmodmanager.widgets import DockWidget


#TODO: maybe open in another windows
#TODO: a mod loader / selector as the one of openMW
class LoadFile(DockWidget):
    uniqueWidget = True
    widgetID = "load"

    def __init__(self, master):
        super(type(self), self).__init__(master)

        self.overLabel = QLabel("Please list the mod you want to load, separated by space. They will be loaded in the order 1 2 3 4 5 ... \nIf you doesn't check the \"create new mod\" box, the last mod in the list will be modified.\navalaible mod: TODO.") #TODO:
        self.edit = QLineEdit() #TODO: allow the use of space
        self.createNewModCheck = QCheckBox("create new mod")
        self.okButton = QPushButton("Ok")
        self.okButton.clicked.connect(self.validate)

        Vbox = QGroupBox() #TODO: find more elegant solution
        self.setWidget(Vbox)

        layout = QVBoxLayout()
        Vbox.setLayout(layout)

        layout.addWidget(self.overLabel)
        layout.addWidget(self.edit)
        layout.addWidget(self.createNewModCheck)
        layout.addWidget(self.okButton)

    def validate(self):
        #TODO: check the validity
        modList = self.edit.text().split(" ")
        if self.createNewModCheck.isChecked():
            modifiedMod = None #TODO: rather create a mod with a path, so we choose were it is saved on creation.
        else:
            modifiedMod = modList[-1]
            modList = modList[0:-1]
        self.master.load_mod(modList, modifiedMod)
        self.close()
