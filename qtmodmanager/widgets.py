from PyQt5.QtWidgets import QDockWidget
from PyQt5.QtCore import Qt


class DockWidget(QDockWidget):
    uniqueWidget = False
    widgetID = None

    def __init__(self, master):
        self.master = master
        QDockWidget.__init__(self, master)

        self.setAllowedAreas(Qt.AllDockWidgetAreas) #TODO: how does this really work
        if self.uniqueWidget:
            assert self.widgetID is not None
            self.master.lock(self.widgetID)

    def close(self):
        QDockWidget.close(self)
        self.master.unlock(self.widgetID)
