from qtmodmanager.bddDisplay import BddDisplay
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel


class BddLink:
    def __init__(self, master, id):
        self.master = master
        self.id = id
        self.dockLoaded = False
        self.modelGenerated = False

        self.bddLoaded = False

    @property
    def bddData(self):
        return self.master.game.ddbs["_database"][self.id]

    @property
    def bdd(self):
        if not self.bddLoaded:
            self._bdd = self.master.game.ddbs[self.id]
            self.bddLoaded = True
        return self._bdd

    def open_widget(self):
        if not self.dockLoaded:
            self.dock = BddDisplay(self.master, self)
            self.master.addDockWidget(Qt.BottomDockWidgetArea, self.dock)#TODO: see documentation for allowed areas
            self.dockLoaded = True

    def generate_model(self):
        #TODO: create a custom model instead
        if not self.modelGenerated:
            self.model = bddModel(self)
            self.modelGenerated = True


class bddModel(QStandardItemModel):
    #TODO: quite ugly, but I doesn't have a good Qt experience... can someone rewrite this, please ?
    def __init__(self, bddLink):
        self.bddLink = bddLink
        super(bddModel, self).__init__(0, len(self.bddLink.bdd.order))
        loop = 0
        for entry in self.bddLink.bdd.order:
            self.setHeaderData(loop, Qt.Horizontal, entry)
            loop += 1

        for elementId in self.bddLink.bdd:
            element = self.bddLink.bdd[elementId]
            self.insertRow(0)
            entryNb = 0
            for entry in self.bddLink.bdd.order:
                self.setData(self.index(0, entryNb), element[entry], isInitialisation=True)
                entryNb += 1

    def setData(self, model, newValue, third=None, isInitialisation=False):
        if not isInitialisation:
            columnNb = model.column()
            rowNb = model.row()
            columnId = self.bddLink.bdd.order[columnNb]
            Id = self.data(self.index(rowNb, 0)) #TODO: do not assume "id" is at 0
            changed = self.bddLink.bdd[Id] #we put this in a variable, as it the id can change
            changed[columnId] = newValue
        return super(type(self), self).setData(model, newValue)
