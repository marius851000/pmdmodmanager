from PyQt5.QtWidgets import QDockWidget, QTableView
from PyQt5.QtCore import Qt


class BddDisplay(QDockWidget):
    def __init__(self, master, bdd):
        self.bdd = bdd
        self.master = master
        super(BddDisplay, self).__init__(self.bdd.id, master)

        self.setAllowedAreas(Qt.AllDockWidgetAreas) #TODO: how does this really work

        self.bddView = QTableView(self)
        self.bddView.setAlternatingRowColors(True)
        self.bddView.setSortingEnabled(True)
        self.bdd.generate_model()
        self.bddView.setModel(self.bdd.model)

        self.setWidget(self.bddView) #TODO:
