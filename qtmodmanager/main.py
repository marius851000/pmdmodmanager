from PyQt5.QtWidgets import QMainWindow, QAction, QErrorMessage, QMessageBox
from modmanager.game import game

from qtmodmanager.bddLink import BddLink
from qtmodmanager.loadFile import LoadFile


class qtmodmanager(QMainWindow):
    def __init__(self):
        super(qtmodmanager, self).__init__()
        self.errorMessageDialog = QErrorMessage(self)

        self.modloaded = False
        self.reserved = {}
        #HACK:
        self.ask_load_mod()
        #self.bddsLink["string"].open_widget()

        self.setWindowTitle("modmanager-qt")

    def create_menus(self): #TODO: refresh menu bar, display them by default
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(QAction(
            "&Open", self, triggered=self.load_mod
        ))
        self.fileMenu.addAction(QAction(
            "&Save", self, triggered=self.save_mod
        ))
        self.create_menu_bdd()

    def create_menu_bdd(self):
        self.categoryMenus = {}

        for bddId in self.game.ddbs["_database"]:
            bddData = self.game.ddbs["_database"][bddId]
            category = bddData["category"]

            if category not in self.categoryMenus:
                menu = self.menuBar().addMenu(category)
                self.categoryMenus[category] = menu

            action = QAction(bddData.id, self, triggered=self.bddsLink[bddId].open_widget)

            self.bddsLink[bddData.id].menuaction = action
            self.categoryMenus[category].addAction(action)

    def ask_load_mod(self):
        if not self.isLocked("load"):
            self.openFileDialog = LoadFile(self)
        else:
            self.errorMessageDialog.showMessage("The file opening dialog is already open.")

    def load_mod(self, mods, modifiedMod=None):
        self.game = game(modifiedMod, mods)
        self.modloaded = True

        self.bddsLink = {}

        for bddId in self.game.ddbs["_database"]:
            bddLink = BddLink(self, bddId)
            self.bddsLink[bddId] = bddLink

        self.create_menus()

    def save_mod(self):
        #TODO: save as, and ask folder
        if not self.modloaded:
            self.errorMessageDialog.showMessage("There is no loaded mod.")
            return
        modToSave = self.game.modifiedMod
        if modToSave.havePath:
            pathToSave = modToSave.path
        else:
            pathToSave = "./testmodifiedmod.json"
        modToSave.save(pathToSave)
        #TODO: rather display this in a status bar
        QMessageBox.information(self, "saved", "mod saved at {}".format(pathToSave))

    def lock(self, id):
        """ add a locker for the specified id """
        if not self.isLocked(id):
            self.reserved[id] = True

    def isLocked(self, id):
        """ return true if the id specified is locked, else otherwise """
        return id in self.reserved

    def unlock(self, id):
        """ remove the locked on the specified id """
        if self.isLocked(id):
            del self.reserved[id]
