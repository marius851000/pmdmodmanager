from lxml import etree
import json
import os

#TODO: rather use the modmanager class directly, and use it to save data.

moveDir = "value/pmd-moves/move_data"
result = {
    "name": "pmd-moves",
    "data": {
        "move": {},
        "string": {}
    }
}

for file in os.listdir(moveDir):
    filePath = os.path.join(moveDir, file)
    fileRead = open(filePath, "r", encoding="1252")
    xmlFile = etree.fromstring(fileRead.read())
    fileRead.close()

    id = int(file.split("_")[0])

    for string in xmlFile.find("Strings"):
        lang = string.tag
        result["data"]["string"]["move_" + str(id) + "_name/" + lang] = {"text": string.find("Name").text}
        result["data"]["string"]["move_" + str(id) + "_info/" + lang] = {"text": string.find("Category").text}

    previousDataDict = None
    for data in xmlFile.findall("Data"):
        dataDic = {}
        for entry in data:
            dataDic[entry.tag] = entry.text
        if previousDataDict is None:
            previousDataDict = dataDic
        else:
            for loop in dataDic:
                if previousDataDict[loop] != dataDic[loop]:
                    raise
    #The two data part are identical

    #assert int(dataDic["MoveID"]) == int(id)
    #raise.

    result["data"]["move"][id] = dataDic


dest = open("mod/pmd-moves.json", "w")
dest.write(json.dumps(result))
dest.close()
