import os
from lxml import etree
#TODO:50 more command line argument, mainly the game for faceFolder

modFolder = "mod"
faceFolder = os.path.join(modFolder, "pmd-face")

xml = etree.fromstring("<mod name=\"pmd-face\"></mod>")
for pokeid in os.listdir(faceFolder):
    for faceid in os.listdir(os.path.join(faceFolder, pokeid)):
        file = os.path.join("pmd-face", pokeid, faceid)
        xml.append(xml.makeelement("data", {"_bdd": "face", "id": pokeid + "|" + faceid.split(".")[0], "file": file}))

f = open(os.path.join(modFolder, "face.xml"), "wb")
f.write(etree.tostring(xml, pretty_print=True))
f.close()
