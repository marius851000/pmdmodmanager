from modmanager.game import game
from modmanager.compiler.pmdexplorer import PmdExplorerCompile

g = game(None, ["game", "mod"])

#import profile
#profile.run("PmdExplorerCompile(g)")
PmdExplorerCompile(g)
