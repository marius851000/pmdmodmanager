from PyQt5.QtWidgets import QApplication

from qtmodmanager.main import qtmodmanager

import sys
app = QApplication(sys.argv)
mainWin = qtmodmanager()
mainWin.show()
sys.exit(app.exec_())
