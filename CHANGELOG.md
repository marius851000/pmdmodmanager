(do not include too initial change, nor minor change)
(Here is the changelog)

#unreleased
## PROJECT
- created a changelog.
## TEST
- Created a test suite. Actually only for game.py and mod.py
- added a basic test for mod loading
## PMD:EXPLORER PARSING
- added ability to parse move
- rewrited the move parser to load json
## PMD:EXPLORER EXPORT
- you can now create a modified rom, just go to the eos-esay-extract folder and type in a shell "make modified.nds"
- move export optimised (now take around 2s on my computer)
## DATABASE
- put a lot of database management mod-side
- database metadata is now saved as a database
- database can now test their entry with limits
- the entry "id" is now auto added to the projects (can be replaced)
- we can now access the lenght of a database, which is cached
- we can now reference a field in another table
- now cache listall
## MOD
- can save and load as json. Depreced XML loading
## GUI
- added a simple GUI
- added the ability to load mod (very basic)
