# TODO
(note: todo are also included in the code, in the way so it can be detected as this by atom extension (or other text editor.). if you can't, just search for TODO, while respecting case)
- test suite for : bdd.py, limits
- A readme file, to explain what it is
- a stable version
 - some sort of documentation (first made a stable version)
- ability to unpack and repack face
- use system (or nix wide) installed program. First, compile ppmdu for linux (should be really hard, with the code in the header)
- understand why I need to copy.copy thing for function call
- in the gui, ensure we can add / delete row, and ensure this is syncronized with others bdd change (that should happen actually)
- remember to rewrite how source is managed
- GUI to create and delete ddb column information
- face to json
- ability to protect a mod from GUI editing
- decide of a way to call column and co
- integrate limits with the GUI
- made a command line command to check database
- test for cache invalidation
