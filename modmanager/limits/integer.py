from modmanager.limit import Limit


class Integer(Limit):
    def verify(self, entryName):
        try:
            int(self._entry_value(entryName))
            return True
        except ValueError:
            return False

    def dump(self):
        return {"type": "Integer"}
