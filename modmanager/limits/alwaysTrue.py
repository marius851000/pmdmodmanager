from modmanager.limit import Limit


class AlwaysTrue(Limit):
    def verify(self, entryName):
        return True

    def dump(self):
        return {"type": "AlwaysTrue"}
