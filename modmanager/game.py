from modmanager.bdd import ddbs
from modmanager.mod import mod as Mod
from modmanager.constant import DEFAULTFILE
import os


class game:
    """this class contain everything that is necessary to modify a game. (like in morrowind)
    step to modify a game:
    - load the game and mod dependancies, keep track of what change what, on multiple layer
    - create/load the entry for our mod, with a priority over others things
    - do the modification to our mod
    - save the modification to our mod. Other mod/game should be changed"""

    def __init__(self, modifiedMod=None, mods=None, paths=None):
        """ initialize this class.
        modifiedMod is the name of the mod that is modified / created
        mods is here to list the mod to load, excluding the modified game (the name, in string))
        path is the list of place to search for mod """
        if mods is None:
            mods = []
        if paths is None:
            paths = ["./"]

        if not (isinstance(modifiedMod, str) or modifiedMod is None):
            raise TypeError
        if not isinstance(mods, list):
            raise TypeError
        if not isinstance(paths, list):
            raise TypeError
        self.ddbs = ddbs(self)
        self.mods = mods
        self.paths = paths
        self.lmods = []
        if modifiedMod is None:
            modifiedMod = Mod(self)
        assert modifiedMod not in self.mods
        self.mods.append(modifiedMod)
        self.load_mods() #TODO:60 path the list of the mod as an argument

    def load_mods(self):
        self.modLoaded = []
        for mod in self.mods:
            self.load_mod(mod)

    @property
    def modifiedMod(self):
        return self.lmods[-1]

    def find_mod_path(self, mod):
        if not isinstance(mod, str):
            raise TypeError
        for path in self.paths:
            actualDir = os.path.join(path, mod)
            if os.path.isdir(actualDir):
                return actualDir
        else:
            raise FileNotFoundError("Impossible to find the mod {} in one of the following path : {}.".format(mod, str(self.paths)))

    def find_file(self, file): #a basic virtual file system
        if not isinstance(file, str):
            raise ValueError
        for mod in self.lmods:
            if mod.havePath:
                actualPath = os.path.join(mod.folder, file)
                if os.path.isfile(actualPath) or os.path.islink(actualPath):
                    return actualPath
        raise FileNotFoundError("file \"%s\" not found in any mods".format(file))

    def load_mod(self, mod):
        if isinstance(mod, str):
            modPath = os.path.join(self.find_mod_path(mod), "default.json")
            thisMod = Mod(self, modPath)
            self.add_mod(thisMod)
        elif isinstance(mod, Mod):
            self.add_mod(mod)
        else:
            raise ValueError

    def add_mod(self, mod):
        if not isinstance(mod, Mod):
            raise ValueError
        if mod not in self.lmods:
            self.lmods.append(mod)

    def list_loadable_mod(self):
        """ return the list of mod that can be loaded """
        loadableMod = []
        for path in self.paths:
            for dir in os.listdir(path):
                #test if this is a valid mod path (check if the file dir/default.json exist)
                if os.path.isfile(os.path.join(dir, DEFAULTFILE)):
                    loadableMod.append(dir)
        return loadableMod
