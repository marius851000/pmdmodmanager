class Limit:
    def __init__(self, game, config=None, bddName="", entryName=""): #TODO:10 better naming
        if config is None:
            config = {}
        self.game = game
        self.bddName = bddName
        self.entryName = entryName

        self.load_from_dict(config)
        self.initialize()

    def initialize(self):
        pass

    def load_from_dict(self, config):
        pass

    def _entry_value(self, entryName):
        return self.game.ddbs[self.bddName][entryName][self.entryName]

    def verify(self, entryName):
        raise NotImplementedError("The function verify() isn't implemented in the limits {}.".format(self))

    def dump(self):
        """ save the limit in a format that can be loaded.
        IMPORTANT: remember to set the type of the limit, as definied in modmanager.limits.__init__.py !!! (for exemple, {"type":"example"}) """
        raise NotImplementedError("The function dump() isn't implemented in the limits {}.".format(self))
