import os
from lxml import etree


# OPTIMIZE:
def PmdExplorerCompile(game, folder="./pmdexplorer"):
    """ generate a folder which may then be compiled """
    os.makedirs(folder, exist_ok=True)
    exportMove(game, folder)


def exportMove(game, folder):
    moveFolder = os.path.join(folder, "move_data")
    os.makedirs(moveFolder, exist_ok=True)
    moveddb = game.ddbs["move"]
    stringddb = game.ddbs["string"]

    # do this for speed optimisation
    stringinfo = {}
    for string in stringddb.beginWith("move_"):
        moveSpecified = string["key"].split("_")[1].split("_")[0]
        if moveSpecified not in stringinfo:
            stringinfo[moveSpecified] = [string]
        else:
            stringinfo[moveSpecified].append(string)

    for moveId in moveddb:
        move = moveddb[moveId]
        moveXml = etree.fromstring("<Move gameVersion=\"EoS\" />") #TODO: get the game version with the mod
        stringXml = moveXml.makeelement("Strings")
        moveXml.append(stringXml)

        for string in stringinfo[moveId]:
            language = string["language"]
            if language not in stringXml:
                stringXml.append(stringXml.makeelement(language))
            stringKey = string["key"]
            if stringKey.endswith("info"):
                id = "Category"
            elif stringKey.endswith("name"):
                id = "Name"
            else:
                raise
            stringXml.find(language).append(stringXml.find(language).makeelement(id))
            stringXml.find(language).find(id).text = string["text"]

        waza_p = moveXml.makeelement("Data")
        moveXml.append(waza_p)
        waza_p2 = moveXml.makeelement("Data")
        moveXml.append(waza_p2)

        for element in move.dump():
            waza_pNewElem = waza_p.makeelement(element)
            waza_p.append(waza_pNewElem)
            waza_pNewElem.text = move[element]
            waza_p2NewElem = waza_p2.makeelement(element)
            waza_p2.append(waza_p2NewElem)
            waza_p2NewElem.text = move[element]

        file = open(os.path.join(moveFolder, moveId + ".xml"), "wb")
        file.write(etree.tostring(moveXml))
        file.close()
