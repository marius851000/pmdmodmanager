from modmanager.load import parseBDD
import os
import json


class mod:
    """ the represention of a modmanager mod. Should mainly access the data on the fly, as the data may be big

    The database specification is stored here, in data["_database"]. It list all the database definied here, with their metadata (name, source & limits...).
    A database is stored like this:
    {
        "name" : databaseName,
        "entrys" : { #also include virtualentry
            "entry1" : { #do not include id
                "limits" : [],
                "source" : None
            }
        }
    }"""

    def __init__(self, game, path=None):
        assert isinstance(path, str) or path is None
        self.loaded = False
        self.game = game
        self._path = path
        self.data = {}
        self.undefinied = {}
        self.includes = []
        if self.havePath:
            self.load()
        else:
            self.loadEmpty()

    def __str__(self):
        result = "<mod " + self.name + " "
        if self.havePath:
            result += "from " + self.path
        else:
            result += "that is virtual (do not have file)"
        result += ">"
        return result

    def loadEmpty(self):
        self.name = "empty mod"
        self.load_finish()

    def load(self):
        self.parse()

        self.includes = []
        if "include" in self.config:
            for include in self.config["include"]:
                self.includes.append(include)
                mod(self.game, os.path.join(self.folder, include))

        self.name = self.config["name"]

        if "ddbs" in self.config:
            for ddbId in self.config["ddbs"]:
                ddb = self.config["ddbs"][ddbId]
                actualBdd = parseBDD(ddb, ddbId, self.game)
                self.addBdd(actualBdd)

        if "data" in self.config:
            for dataId in self.config["data"]:
                self.data[dataId] = self.config["data"][dataId]

        self.load_finish()

    def load_finish(self):
        self.game.add_mod(self)
        self.loaded = True

    @property
    def path(self):
        if self._path is None:
            raise ValueError("The mod {} doesn't have a path".format(str(self)))
        return self._path

    @property
    def folder(self):
        return os.path.dirname(self.path)

    @property
    def havePath(self):
        if self._path is not None:
            return True
        return False

    def delete(self, bdd, id):
        self.undefine(bdd, id)
        if bdd in self.data:
            if id in self.data[bdd]:
                del self.data[bdd][id]

    def undefine(self, bdd, id):
        if bdd not in self.undefinied:
            self.undefinied[bdd] = {}
        self.undefinied[bdd][id] = True

    def isDefinied(self, bdd, id):
        #if self.exist(bdd, id):
        #    return True
        if bdd in self.undefinied:
            if id in self.undefinied[bdd]:
                if self.undefinied[bdd][id] is True:
                    return False
        return True

    def exist(self, bdd, id):
        if bdd in self.data:
            if id in self.data[bdd]:
                return True
        return False

    def get(self, bdd, id):
        if self.exist(bdd, id):
            return self.data[bdd][id]
        raise ValueError("[{}][{}] isn't in {}.data".format(bdd, id, self))

    def redefine(self, bdd, id):
        if bdd in self.undefinied:
            if id in self.undefinied[bdd]:
                del self.undefinied[bdd]

    def assign(self, bdd, id, content):
        assert isinstance(content, dict)
        self.redefine(bdd, id)
        if bdd not in self.data:
            self.data[bdd] = {}
        self.data[bdd][id] = content

    def addBdd(self, data):
        id = data["name"]
        del data["name"]
        self.assign("_database", id, data)

    def parse(self):
        f = open(self.path, "rb")
        self.config = json.loads(f.read())
        f.close()

    def dump(self):
        """ create a dump of the mod, that could be loaded """
        database = None
        ddbs = {}
        data = self.data.copy()
        if "_database" in data:
            database = data["_database"]
            del data["_database"]

        if database is not None:
            for ddbId in database:
                ddb = database[ddbId]
                ddbs[ddbId] = {
                    "entrys": {}
                }
                for entryId in ddb["entrys"]:
                    if entryId != "id": #TODO: see how to add custom limits to "id"
                        entry = ddb["entrys"][entryId]
                        ddbs[ddbId]["entrys"][entryId] = {}
                        if "limits" in entry:
                            if len(entry["limits"]) > 0:
                                ddbs[ddbId]["entrys"][entryId]["limits"] = []
                                for limit in entry["limits"]:
                                    ddbs[ddbId]["entrys"][entryId]["limits"].append(limit.dump())

                        if "source" in entry:
                            if entry["source"] is not None:
                                ddbs[ddbId]["entrys"][entryId]["source"] = entry["source"]

                if "category" in ddb:
                    ddbs[ddbId]["category"] = ddb["category"]

        #TODO: do not add if dict is empty
        dump = {
            "ddbs": ddbs,
            "data": data,
            "removed": self.undefinied.copy(),
            "name": self.name,
            "include": self.includes
        }
        return dump

    def save(self, path):
        """ save the content of the mod in a format that can be loaded as a mod, at the file path. """
        f = open(path, "w")
        f.write(json.dumps(self.dump(), indent=2))
        f.close()
