import modmanager.limits.__init__ as limits
import inspect


def parseBDD(config, DDBId, game):
    data = {
        "entrys": {},
        "name": DDBId,
        "order": [],
    }

    if "category" in config:
        data["category"] = config["category"]
    else:
        data["category"] = "unknown"

    for entryId in config["entrys"]:
        entry = config["entrys"][entryId]
        tempEntry = {}
        if "limits" in entry:
            tempEntry["limits"] = []
            for limit in entry["limits"]:
                tempEntry["limits"].append(parseLimit(game, limit, DDBId, entryId))
        if "source" in entry:
            tempEntry["source"] = entry["source"] #TODO:30 make source like limit
        if "type" in entry:
            tempEntry["type"] = entry["type"]
        else:
            tempEntry["type"] = "normal"
        data["entrys"][entryId] = tempEntry
        data["order"].append(entryId)
    return data


def parseLimit(game, entryConfig, bddName, entryName):
    classes = inspect.getmembers(limits, inspect.isclass)
    awaitedName = entryConfig["type"][0].upper() + entryConfig["type"][1:]
    for name, classe in classes:
        if name == awaitedName:
            limit = classe(game, entryConfig, bddName, entryName)
            return limit
    raise ValueError("no limit '{}' found".format(awaitedName))
