import copy


class ddbs:
    """ manage all the bdd """
    def __init__(self, game):
        self.game = game
        self.database = Bdd(game, "_database")

    def __getitem__(self, id):
        """ get the database named 'id' """
        return Bdd(self.game, id)


class Bdd:
    """ a bdd. It has multiple layer. When asking, something, it answer with the hightest answer avalaible. When seting something, it modify the database for the defined "modifiedMod". When deleting something, it is marked as deleted """
    def __init__(self, game, name):
        self.name = name
        self.game = game
        self.lenComputed = False #TODO: a better caching system, that deload thing that are not commonly used
        self.listComputed = False

    def refresh(self):
        self.lenComputed = False
        self.listComputed = False

    def addEntry(self, name, entry):
        """ add the row 'name' with the content 'entry' (should be a dict with all the value) """
        self.entrys[name] = entry

    def getModSet(self, item):
        """ get the mod that contain the entry 'item' """
        result = None
        for mod in self.game.lmods:
            if not mod.isDefinied(self.name, item):
                result = None
            if mod.exist(self.name, item):
                result = mod
        if result is not None:
            return result
        raise AttributeError("\"{}\" not found in the database {} in one of those mod {}.".format(item, self, self.game.lmods))

    def listall(self):
        """ list every row in this ddb """
        if self.listComputed:
            return self.lastList
        result = []
        for mod in self.game.lmods:
            if self.name in mod.undefinied:
                for item in mod.undefinied[self.name]:
                    if mod.undefinied[self.name][item]:
                        result.remove(item)
            if self.name in mod.data:
                for item in mod.data[self.name]:
                    if item not in result:
                        result.append(item)
        self.lastList = result
        self.listComputed = True
        return result

    def len(self):
        """ return the number of element of the ddb """
        if self.lenComputed:
            return self.lastLen
        self.lenComputed = True
        self.lastLen = len(self.listall())
        return self.lastLen

    def isValid(self):
        """ return True if the database appear to be correcly loaded, False otherwise """
        if self.name in self.game.ddbs["_database"].listall():
            return True
        else:
            return False

    def __getitem__(self, name):
        """ get the element 'name' in the database """
        if isinstance(name, str):
            self.getModSet(name) #test if the name is in a database
            return dataWrapper(self, name)
        raise ValueError("{} should be a string.".format(name))

    def __iter__(self):
        """ return an iterator, that iterate throught the name of all row in the database """
        generatedList = self.listall()
        return generatedList.__iter__()

    def __str__(self):
        """ return a string representation of the ddb object """
        result = "<database \""
        result += self.name
        result += "\" with entrys "
        for entry in self.entrys:
            result += "\"" + entry + "\", "
        result = result[:-2]
        result += ">"
        return result

    def beginWith(self, what):
        result = []
        for id in self.listall():
            if id[0:len(what)] == what:
                result.append(self[id])
        return result

    def check(self):
        """ check if the database is properly formated, according to limits."""
        errors = []
        for element in self:
            errors.extend(self[element].check())
        return errors

    @property
    def entrys(self):
        """ return the list of entrys of this database """
        if self.name[0] == "_":
            if self.name == "_database":
                return { #TODO: move this to a special file (or maybe a JSON file)
                    "entrys": {"type": "normal"},
                    "category": {"type": "normal"},
                    "order": {"type": "normal"}
                }
            else:
                raise KeyError("The special database '{}' isn't hard-coded.".format(self.name))
        return self.game.ddbs.database[self.name]["entrys"]

    @property
    def order(self):
        """ return the order of entry in the database. When possible, don't use order for database entry """
        if self.name[0] == "_":
            raise #TODO:
        else:
            elements = self.game.ddbs.database[self.name]["order"]
        result = ["id"]
        result.extend(elements)
        return result


class dataWrapper:
    def __init__(self, bdd, id):
        self.id = id
        self.bdd = bdd

    def __getitem__(self, item):
        if item == "id":
            return self.id
        else:
            entry = self.bdd.entrys[item]
            if entry["type"] == "normal":
                rawdict = self.dump()
                return rawdict[item]
            elif entry["type"] == "virtual":
                #TODO:100 use a POO thing here
                source = entry["source"]
                sourceType = source["type"]
                if sourceType == "same":
                    return self[source["entry"]].split(source["separator"])[source["number"]]
                elif sourceType == "other":
                    idToGet = (source.get("preId") or "") + self["id"] + (source.get("postId") or "")
                    return self.bdd.game.ddbs[source["table"]][idToGet][source["displayColumn"]]
                else:
                    raise
            else:
                raise ValueError("error on the database {} with the entry '{}'.".format(self, item))

    def __setitem__(self, item, value):
        self.bdd.refresh()
        setmod = self.bdd.game.modifiedMod
        rawdict = self.dump()
        if item == "id":
            #TODO:20 check if this overwrite a value
            setmod.assign(self.bdd.name, value, rawdict)
            setmod.delete(self.bdd.name, self.id)
            self.id = value
        else:
            rawdict[item] = value
            setmod.assign(self.bdd.name, self.id, rawdict)

    def dump(self):
        return copy.copy(self.bdd.getModSet(self.id).data[self.bdd.name][self.id])

    def check(self):
        """check if this entry is properly formatted, according to limits"""
        errors = []
        #TODO: a more advanced return type
        entrys = self.bdd.entrys
        #TODO: check for missing entry
        for entryId in entrys:
            entry = entrys[entryId]
            if "limits" in entry:
                for limit in entry["limits"]:
                    result = limit.verify(self.id)
                    if not result:
                        if isinstance(result, str):
                            errors.append(result)
                        else:
                            errors.append("the limits {} is not valid for {}.".format(limit, self.id))
        return errors
