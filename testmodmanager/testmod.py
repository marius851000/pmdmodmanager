# test the mod class
import unittest
import testmodmanager.constant as constant

import os
from modmanager.game import game
from modmanager.mod import mod as Mod


class testMod(unittest.TestCase):
    """ for the mod class """
    def getEmptyMod(self):
        g = game()
        return Mod(g)

    def getModWithPath(self):
        g = game()
        return Mod(g, os.path.join(constant.modFolder, "emptymod", "default.json"))

    def testModCreation(self):
        self.getEmptyMod()
        self.getModWithPath()

    def testMod__str__(self):
        str(self.getEmptyMod())

    def testLoadMod(self):
        g = game()
        mod = Mod(g, os.path.join(constant.modFolder, "testmod", "default.json"))
        assert mod.name == "testmod"
        #TODO: check limits and category
        assert g.ddbs["DDBtest"].isValid()
        assert type(g.ddbs["DDBtest"]["line"].dump()) == dict
        assert g.ddbs["DDBtest"]["line"]["entry1"] == 1
        assert g.ddbs["DDBtest"]["line"]["entry2"] == "test2"
        #TODO: check removed

    #TODO: check save

    def testModPath(self):
        try:
            self.getEmptyMod().path
        except ValueError:
            pass
        self.getModWithPath().path

    def testFolder(self):
        self.getModWithPath().folder

    def testHavePath(self):
        self.assertFalse(self.getEmptyMod().havePath)
        self.assertTrue(self.getModWithPath().havePath)

    #BDD related test

    def testDelete(self):
        m = self.getEmptyMod()
        m.assign("bdd", "id", {})
        m.delete("bdd", "id")
        self.assertRaises(ValueError, m.get, "bdd", "id")

    def testDefine(self):
        m = self.getEmptyMod()
        m.undefine("bdd", "id")
        self.assertFalse(m.isDefinied("bdd", "id"))

        m = self.getEmptyMod()
        m.assign("bdd", "id", {})
        m.undefine("bdd", "id")
        m.get("bdd", "id")

    def testExist(self):
        m = self.getEmptyMod()
        m.assign("bdd", "id", {})
        self.assertTrue(m.exist("bdd", "id"))

        self.assertFalse(self.getEmptyMod().exist("bdd", "id"))

    def testGet(self):
        m = self.getEmptyMod()
        d = {"test": True}
        m.assign("bdd", "id", d)
        self.assertEqual(m.get("bdd", "id"), d)

        self.assertRaises(ValueError, self.getEmptyMod().get, "bdd", "id")

    #TODO: test parse
