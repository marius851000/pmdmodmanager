import unittest
import testmodmanager.constant as constant

import os
from modmanager.mod import mod as Mod
from modmanager.game import game


class testGame(unittest.TestCase):
    """ for the game function """
    def getGameInstance(self):
        return game("emptymod", paths=constant.modFolders)

    def testCreation(self):
        game()

    def testCreationWithLoad(self):
        self.getGameInstance()

        #weird behavior were default function argument argument arent copied
        game(mods=["emptymod"], paths=constant.modFolders)
        game("emptymod", paths=constant.modFolders)

        game(None, ["emptymod"], paths=constant.modFolders)

    def testCreationWithError(self):
        self.assertRaises(TypeError, game, 10)
        self.assertRaises(TypeError, game, b"10")
        self.assertRaises(TypeError, game, mods="error")
        self.assertRaises(TypeError, game, paths="error")

    def testModifiedMod(self):
        self.getGameInstance().modifiedMod

    def testFindModPath(self):
        assert os.path.isdir(self.getGameInstance().find_mod_path("emptymod"))
        self.assertRaises(FileNotFoundError, self.getGameInstance().find_mod_path, "afnuzfnzeifjzi")
        self.assertRaises(TypeError, self.getGameInstance().find_mod_path, 10)

    def testFindFile(self):
        self.getGameInstance().find_file("default.json")
        self.assertRaises(FileNotFoundError, self.getGameInstance().find_file, "fiaejfincuxenufherfsd")
        self.assertRaises(ValueError, self.getGameInstance().find_file, 10)

    def testload_mod(self):
        g = self.getGameInstance()
        g.load_mod(Mod(g))

        game(paths=constant.modFolders).load_mod("emptymod")

        self.assertRaises(ValueError, self.getGameInstance().load_mod, 10)

    def testadd_mod(self):
        g = self.getGameInstance()
        g.add_mod(Mod(g))

        self.assertRaises(ValueError, self.getGameInstance().add_mod, 10)
